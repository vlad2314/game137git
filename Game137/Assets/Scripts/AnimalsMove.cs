using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AnimalsMove : MonoBehaviour
{
    public GameObject target;
    private NavMeshAgent meshAgent;
    private Vector3 AnimalsPos;
    // Start is called before the first frame update
    void Start()
    {
        meshAgent = GetComponent<NavMeshAgent>();
    }
    // Update is called once per frame
    void Update()
    {
        meshAgent.destination = target.transform.position;
        var a = target.transform.position - transform.position;
        AnimalsPos = transform.position;
        if (Mathf.Abs(a.z) < 3 || target.transform.position.y < -2)
        {
            target.transform.position = new Vector3(Random.Range(AnimalsPos.x -30, 
                AnimalsPos.x + 30), 1, Random.Range(AnimalsPos.z - 30, AnimalsPos.z + 30));
        }

    }
}
