﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsList
{
    public string Name;
    public int Price;

    public ItemsList(string name, int price)
    {
        Name = name;
        Price = price;
    }
}
public class Items : MonoBehaviour
{
    public List<string> PlayerItems;
    public List<ItemsList> items;
    private void Start()
    {
        items = new List<ItemsList>() { new ItemsList("logo", 100) };
    }
    public void AddListItems(string name)
    {
        PlayerItems.Add(name);
    }
}
