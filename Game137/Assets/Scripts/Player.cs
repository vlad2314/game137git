using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public GameObject SceneMenu;
    private bool flagMenu;
    private GameObject Menu;
    // Start is called before the first frame update
    void Start()
    {
        flagMenu = false;  
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape) && !flagMenu)
        {
            Menu = Instantiate(SceneMenu);
            flagMenu = true;
        }
        else if(Input.GetKeyDown(KeyCode.Escape) && flagMenu)
        {
            Destroy(Menu);
            flagMenu = false;
        }
    }
}
