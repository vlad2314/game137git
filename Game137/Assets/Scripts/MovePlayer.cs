﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{
    public float Speed = 5f;
    public float JumpForce = 300f;
    private bool _isGrouded;
    private Rigidbody _rb;
    private bool _flagLifting;
    private GameObject Item;



    void Start()
    {
        _rb = GetComponent<Rigidbody>();
    }
    
    private void JumpLogic()
    {
        if(Input.GetAxis("Jump")> 0)
        {
            if (_isGrouded)
            {
                _rb.AddForce(Vector3.up * JumpForce);
            }
        }
    }
    private void IsGroundedUpdate(Collision collision, bool value)
    {
        if(collision.gameObject.tag == ("Ground"))
        {
            _isGrouded = value;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        IsGroundedUpdate(collision, true);
    }
    private void OnCollisionExit(Collision collision)
    {
        IsGroundedUpdate(collision, false);
    }
    private void MoveLogic()
    {
        float moveH = Input.GetAxis("Horizontal");
        float moveV = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(moveH, 0.0f, moveV);
        transform.Translate(movement * Speed * Time.deltaTime);
    }
    private void Lifting()
    {
        if(Input.GetKeyDown(KeyCode.F) && _flagLifting)
        {
            _flagLifting = false;
            print(Item.name);
            GameObject.Find("Items").GetComponent<Items>().AddListItems(Item.name);
            Destroy(Item);
        }
    }
    void FixedUpdate()
    {
        MoveLogic();
        JumpLogic();
        Lifting();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Item")
        {
            _flagLifting = true;
            Item = other.gameObject;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Item")
        {
            _flagLifting = false;
            Item = null;
        }
    }
}
