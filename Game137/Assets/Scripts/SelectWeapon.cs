using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectWeapon : MonoBehaviour
{
    public List<GameObject> inventoryPlayer = new List<GameObject>(3);
    public int Weapon = 1;
    public GameObject Hand;
    private void Start()
    {
        Hand = inventoryPlayer[Weapon - 1];
    }
    void Update()
    {
        if (Input.GetKey(KeyCode.Alpha1))
        {
            Weapon = 1;

        }
        if (Input.GetKey(KeyCode.Alpha2))
        {
            Weapon = 2;
        }
        if (Input.GetKey(KeyCode.Alpha3))
        {
            Weapon = 3;
        }
        Hand = inventoryPlayer[Weapon - 1];
    }
}
