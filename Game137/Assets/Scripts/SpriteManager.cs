﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteManager : MonoBehaviour
{
    public List<Sprite> sprites;
    public List<Sprite> SelectInfentory(List<string> nameSprites)
    {
        List<Sprite> ListSprites = new List<Sprite>();
        for (int i = 0; i < nameSprites.Count; i++)
        {
            foreach (var item in sprites)
            {
                if(nameSprites[i] == item.name)
                {
                    ListSprites.Add(item);
                    break;
                }
            }
        }
        return ListSprites;
    }
}
