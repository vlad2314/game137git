using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Looker : MonoBehaviour
{
    public GameObject guard;

    private float reset = 5;
    private bool movingDown;


    // Update is called once per frame
    void Update()
    {
        if (!movingDown)
        {
            //transform.position -= new Vector3(0f, 0f, 0.1f);
        }
        else
        {
            //transform.position += new Vector3(0f, 0f, 0.1f);
        }
        if(transform.position.z > 10)
        {
            movingDown = false;
        }
        else if(transform.position.z < -10)
        {
            movingDown = true;
        }
        reset -= Time.deltaTime;
        if(reset <=0)
        {
            GetComponent<AnimalsMove>().enabled = false;
            guard.GetComponent<BoxCollider>().enabled = true;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Point")
        {
            GetComponent<AnimalsMove>().enabled = true;
            reset = 5;
            guard.GetComponent<BoxCollider>().enabled = false;
        }
    }
}
