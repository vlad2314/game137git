﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Dialog : MonoBehaviour
{
    public Text textNameUI;
    public Text textDialogUI;

    private string[] textDialog;

    private string InputText;
    private int _index = -1;
    private bool _flag = false;

    private void OnClick()
    {
        string nameButton = EventSystem.current.currentSelectedGameObject.name;
        InputText = nameButton;
        _flag = true;
        
    }
    public void StartDialog(string Name, string[] dialog)
    {
        textNameUI.text = Name;
        textDialog = dialog;
        _index = 0;
        _flag = true;
    }
    // Update is called once per frame
    void Update()
    {
        if (_index == -1) return;
        if (_flag)
        {
            _flag = false;
            textDialogUI.text = textDialog[_index];
            _index++;
        }
    }
}
