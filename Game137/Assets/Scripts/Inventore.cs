﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventore : MonoBehaviour
{
    public List<Image> images; 
    // Start is called before the first frame update
    void Start()
    {
        
        var sprites = GetComponent<SpriteManager>().SelectInfentory(GameObject.Find("Items").GetComponent<Items>().PlayerItems);
        for (int i = 0; i < sprites.Count; i++)
        {
            images[i].sprite = sprites[i];
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            Destroy(gameObject);
        }
    }
}
