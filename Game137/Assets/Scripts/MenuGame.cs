using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuGame : MonoBehaviour
{
    private bool flagGame;
    public Text TextOne;
    public void NewGame()
    {
        if (flagGame)
        {
            Menu.OnePlay = false;
            Menu.MenuText = "Продолжить";
            SceneManager.LoadScene("Tutorial");
        }
        else
        {
            Destroy(gameObject);
        }
    }
    public void ConnGame()
    {

    }
    public void SettGame()
    {

    }
    public void ExitGame()
    {
        Application.Quit();
    }
    // Start is called before the first frame update
    void Start()
    {
        TextOne.text = Menu.MenuText;
        flagGame = Menu.OnePlay;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
