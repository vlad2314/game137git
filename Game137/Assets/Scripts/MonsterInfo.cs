﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterInfo : MonoBehaviour
{
    public float HP = 100;
    public float Speed = 10;
    public float Damage = 10;
    public float Armor = 10;
}
