﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerKill : MonoBehaviour
{
    private bool flagKill = false;
    private float DamagePlayer = 10;
    private GameObject gameMonster;
    private void OnTriggerEnter(Collider other)
    {
        gameMonster = other.gameObject;   
    }
    private void OnTriggerExit(Collider other)
    {
        gameMonster = null;
    }
    private void Update()
    {
        DamagePlayer = PlayerInfo.DamagePlayer;
        if (Input.GetKey(KeyCode.Mouse0) && gameMonster != null)
        {
            if (gameMonster.tag == "Monster")
            {
                gameMonster.GetComponent<MonsterInfo>().HP -= DamagePlayer;
            }
        }
    }
}
